import { Component } from "react";
import Logo from "../images/icons/lifyLogo.png";
import Up from "../images/icons/up.png";
import Empty from "../images/icons/empty.png";
import "../css/MainPage.css"
import {findLyricApi} from '../api/apiList';

export default class MainPage extends Component{
    constructor(props){
        super(props)
        this.state = {
            waw2: "Yuhu",
            songLyrics: "",
            inputArtist: "",
            inputSong: "",
            screenWidth: 0,
            screenHeight: 0,
            mobileView:false,
            scroll: 0,
        }
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.updateScrollPos = this.updateScrollPos.bind(this);
    }

    componentDidMount(){
        this.updateWindowDimensions()
        this.updateScrollPos()
        window.addEventListener('resize', this.updateWindowDimensions);
        window.addEventListener('scroll', this.updateScrollPos);
    }

    componentWillUnmount(){
        window.removeEventListener('resize', this.updateWindowDimensions);
        window.addEventListener('scroll', this.updateScrollPos);
    }

    updateScrollPos(){
        this.setState({
            scroll: window.pageYOffset
        })
    }

    updateWindowDimensions() {
        if(window.innerWidth < 600){
            this.setState({
                mobileView:true
            })
        }
        else{
            this.setState({
                mobileView:false
            })
        }
    

    }

    render(){
        return(
            <div className="Main-page">
                {this.newHeader()}
                <div className="Top-bar">
                    <div className="Top-bar-text">
                        <a className="BigText-text">
                            Find the lyrics of your favorite song!
                        </a>
                    </div>
                    {this.renderTopBar()}
                </div>
                <div style={{height:this.state.songLyrics.length<1?'100vh':''}} className="Lyrics-bar">
                    <a>{this.state.songLyrics.replace(/(?:\\[rn])+/g, "")}</a>
                </div>
                {this.buttoScrollToTop()}
                <div style={{position:this.footerControl()}} className='Footer'>
                    <a><a style={{color:'white'}} href={'https://bitbucket.org/michmadheo/lify/src/master/'}>Lify Open Source</a>, <a style={{color:'white'}} href={'https://www.linkedin.com/in/michael-amadheo-211184171/'}>Michael Amadheo</a> 2021, API used made by <a style={{color:'white'}} href={'https://lyricsovh.docs.apiary.io/#reference/0/lyrics-of-a-song/search'}>Basile Bruneau.</a> </a>
                </div>
            </div>
        )
    }

    newHeader(){
        return(
            <div className='Header'>
                <img className="Header-main-logo" src={Logo}/>
            </div>
        )
    }

    buttoScrollToTop(){
        if(this.state.scroll>50){
            return(
                <div>
                    <div onClick={()=>{this.scrollToTop()}} className='Back-to-top'>
                        {/* {this.state.scroll} */}
                        <img className="Back-to-top-image" src={Up}/>
                    </div>
                </div>
            )
        }
    }

    renderLyrics(){
        if(this.state.songLyrics.length<1){
            return(
                <img className="Image-empty" src={Empty}/>
            )
        }
        else{
            return(
                <a>{this.state.songLyrics.replace(/(?:\\[rn])+/g, "")}</a>
            )
        }
    }

    renderTopBar(){
        if(this.state.mobileView){
            return(
                <div className="Top-bar-input-mobile">
                    <input type="Search" 
                        value={this.state.inputArtist}
                        onChange={(e)=>{this.setState({inputArtist:e.target.value})}}
                        placeholder={'Type the Singer/Band/Artist here'}
                        style={inputMobileView}/>
                    <input type="Search" 
                        value={this.state.inputSong}
                        onChange={(e)=>{this.setState({inputSong:e.target.value})}}
                        placeholder={'Type a Song here'}
                        style={inputMobileView}/>
                    <button
                            onClick={()=>{this.findLyrics()}} 
                            style={{
                                width:'40%',
                                backgroundColor:this.buttonColorControl(), 
                                border:'none', 
                                borderRadius:10, 
                                color:'white', 
                                height:50,
                                outline: 'none',
                                fontSize:20,
                                fontWeight:'bold',
                                marginTop:20,
                            }}>Find</button>
                </div>
            )
        }
        else{
            return(
                <div className="Top-bar-input-web">
                    <input type="Search" 
                        value={this.state.inputArtist}
                        onChange={(e)=>{this.setState({inputArtist:e.target.value})}}
                        placeholder={'Type the Singer/Band/Artist here'}
                        style={inputWebView}/>
                    <input type="Search" 
                        value={this.state.inputSong}
                        onChange={(e)=>{this.setState({inputSong:e.target.value})}}
                        placeholder={'Type a Song here'}
                        style={inputWebView}/>
                        <button
                            onClick={()=>{this.findLyrics()}} 
                            style={{
                                width:100, 
                                marginLeft:20, 
                                backgroundColor:this.buttonColorControl(), 
                                border:'none', 
                                borderRadius:10, 
                                color:'white', 
                                height:40,
                                outline: 'none',
                                fontSize:15,
                                fontWeight:'bold',
                                marginTop:20,
                            }}>Find</button>
                </div>
            )
        }
    }

    footerControl(){
        if(this.state.songLyrics.length<1){
            return 'relative'
        }
        else{
            return 'relative'
        }
    }

    scrollToTop(){
        window.scrollTo({top: 0, behavior: 'smooth'});
    }

    buttonColorControl(){
        if(this.state.inputArtist.length < 1 || this.state.inputSong.length < 1){
            return '#ffd4d4'
        }
        else{
            return '#ff5050'
        }
    }

    async findLyrics(){
        if(this.state.inputArtist.length>1 && this.state.inputSong.length>1){
            let artist = await this.state.inputArtist.replace(/\s/g, '%20')
            let song = await this.state.inputSong.replace(/\s/g, '%20')

            fetch(findLyricApi+artist+'/'+song)
            .then((response) => response.json())
            .then((json) => {
                if(json.lyrics.length > 0){
                    this.setState({
                        songLyrics: json.lyrics
                    })
                }
                else{
                    this.setState({
                        songLyrics: ""
                    })
                }
            })
            .catch((error) => {
                console.log(error)
            });   
        }
      };
}

const inputWebView ={
    width:"25%",
    background:"white", 
    border:"none", 
    marginTop:20,
    padding:15,
    marginLeft:10,
    marginRight:10, 
    fontSize:20,
    outline: 'none',
    borderRadius:10
}

const inputMobileView ={
    width:"80%",
    background:"white", 
    border:"none", 
    marginTop:20,
    padding:15, 
    fontSize:20,
    outline: 'none',
    borderRadius:10
}